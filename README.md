Ansible Role repo-umd4
======================

Install [EGI UMD4](http://repository.egi.eu/category/umd_releases/distribution/umd-4/) repository

Requirements
------------

* EL6/7

Role Variables
--------------

  default/main.yaml

     repo_umd4_priority: 20


Example Playbook
----------------

    - hosts: servers
      roles:
         - hephyvienna.repo-umd4

License
-------

MIT

Author Information
------------------

Written by [Dietrich Liko](Dietrich.Liko@oeaw.ac.at) in March 2019

[Institute for High Energy Physics](http://www.hephy.at) of the
[Austrian Academy of Sciences](http://www.oeaw.ac.at)
