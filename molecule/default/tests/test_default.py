import os

import testinfra.utils.ansible_runner
import pytest

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.parametrize("name", [
    'UMD-4-base.repo',
    'UMD-4-updates.repo'
])
def test_repo_file(host, name):
    f = host.file(os.path.join('/etc/yum.repos.d', name))

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'


def test_yum_cache(host):
    vars = host.ansible.get_variables()
    print(vars)
    f = host.file('/var/cache/yum/x86_64/')
    assert f.exists
    assert f.is_directory
